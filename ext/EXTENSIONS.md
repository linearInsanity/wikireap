WikiReap Extentions
======

###What is the Goal of Extentions?

Rather than bloat the Main WikiReap code, the goal of
Extensions is to provide a modular approach to using
WikiReap. By using extensions rather than a unified
codebase, it allows for the use of ambiguous context
when executing part of the code. For example, the 'Paraphrase'
extension can paraphrase any text passed to it, rather
than just WikiReap output.

Note that the code below will not work in all contexts
and simply serves as an example.

```
echo "Please Paraphrase this Text" | paraphrase
```

###What Are Some Extentions?

######Paraphrase

Paraphrase is an extension for WikiReap which takes
text through [STDIN] and converts the text into a
coherent equivalent sentence. The output is sent through
[STDOUT] and any errors are reported through [STDERR].
On systems lacking such files, everything is output
through the standard system output. 

Example used with WikiReap to [STDOUT]:
```
$ wikireap -u "http://en.wikipedia.com/wiki/Dennis_Ritchie" | paraphrase

Dennis Ritchie was a ...
```
 
Example used with Echo or other Pipe to file:
```
$ echo "Please paraphrase this good sentence" | paraphrase > pp.txt
$ echo pp.txt

Possibly reword this nice sentence
```

######WikiReap-Qt

WikiReap-Qt is less of an Extension but more of a standalone application
to create a Desktop interface for Mac, Windows, Linux and BSD. WikiReap-Qt
makes the appropriate calls to WikiReap and extensions to deliver an exactly
similar experience to the CLI in a GUI environment

######WikiSearch

WikiSearch is an Extension the takes a query from [STDIN], inputs the
query in WikiPedia, Resolves to the article, and returns the corresponding
URL. This is done to make it easier to get article from WikiPedia and makes
WikiReap more efficient.

```
$ wikisearch "Dennis Ritchie" | wikireap -o "dmr.txt"
$ echo dmr.txt

Dennis MacAlistar Ritchie was born...
```
