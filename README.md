#WikiReap Documentation

###About Wikireap

WikiReap is a Command Line Interface for 
scraping Wiki Pages and other structured
websites. WikiReap is built in Python, so
most of the code is portable; however,
there are builds for each platform to deal
with specific nuances of each individual
system. WikiReap is available on the Google
Play Store as well as on the Apple App Store
as "WikiReap" by "Chord Development Studios
LLC.".

###Dependencies

Wikireap depends on a few modules. Some may come
already installed on your system, or you may find
that these dependencies have not been met yet. If
you wish to avoid having to worry about dependencies,
there are binary builds in the respective '/bin/'
folder. The dependencies are as follows:

```
OptionParser: Used to Parse Flags and Arguments Sent to WikiReap
BeautifulSoup4: Used to Parse Retrieved Websites
Requests: Used to Establish Connection and Download Website
Re: Used to Parse Document using Regular Expression Replace
Codecs: Used to Support Unicode Encoding for Websites and Output
Textwrap: Used to Beautify Output
```

###Command Line Arguments

To run WikiReap, see your platform specific
instruction in "/bin/{platform}/README.md"
The following are the flags and options that
WikiReap accepts.

```
-u, --url .................. Page to Scrap
-o, --output ........ Output File Location
-d, --delimit ...... Line Return Delimiter
-p, --pdf ......... Compile PDF from Latex
-w, --html ........... Output Web Document
-l, --latex ............ Output Latex File
-v, --verbose ....... Enables Verbose Mode
-a, --about .................. Print About
```

###License

This work is licensed under the GPLv3
(General Public License). For more info,
please read license.txt.
