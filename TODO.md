Todo
======
###Publishing
```
> Modify Source for Mac, Windows, Android, iOS, and BSD
> Build Binaries for Mac, Windows, Android, iOS, and BSD
```

###Development
```
> Clean Up Code (Specifically OptionParser Code)
> Add More Exception Handling
> Add Conditional Import Code
> Possibly Remove Verbose Mode (Superfluous and Useless Bloat)
```

###Documentation
```
> Add Platform Specific Documentation
> Plugin Documentation
```
